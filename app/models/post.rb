class Post < ActiveRecord::Base
	# no foreign key - no special column needed
	validates :title, :presence => true
	validates :body, :presence =>true
	has_many :comments, :dependent => :destroy
	belongs_to :user
end
