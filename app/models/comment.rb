class Comment < ActiveRecord::Base
	# foreign key - requires a database column: post_id of type: integer
	validates :post_id, :presence => true
	validates :body, :presence => true
	belongs_to :post
	belongs_to :user
end
